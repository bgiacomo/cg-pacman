/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  * 
*                                                                               *
*                   Pacman simulator - Computer Graphics                        *
*                                                                               *
*                   di Bartoli Giacomo, Sapignoli Michele                       *
*   Alma Mater Studiorum - Ingegneria e Scienze Informatiche - Sede di Cesena   *
*             Corso: Computer Graphics; Dott.ssa Damiana Lazzaro                *
*                                                                               *
*                         Sviluppato con Visual Studio 2010                		*
*                                                                               *
*                                                                               *
*  La grafica bidimensionale, sviluppata grazie alle librerie openGL, si presta *
*  molto bene a ricreare piccole simulazioni dei primi videogiochi anni 80.     *
*  Da qui nasce l'idea di cimentarsi nel tentativo di simulare un specie di     *
*  Pacman, un po' più semplificato.												*
*                                                                               *
*	Una volta avviato il gioco sarà possibile muovere pacman usando le freccie	*
*   di direzione. I fantasmi appaiono solo dopo che Pacman ha fatto un certo 	*
*	numero di passi, questo per dare un piccolo vantaggio all'utente. I fantasmi*
*	si muovo continuamente lungo un percorso prestabilito. Se Pacman incrocia i	*
*	fantasmi l'utente perde: si azzera il punteggio e si rincomincia da capo. 	*
*	E' possibile tramite il tasto 'R' resettare il gioco. In caso di vittoria,	*
*	appare una scritta al centro dello schermo lampeggiante. Per renderlo più	*
*	simile all'originale sono stati aggiunti anche files audio.					*
*	Tutto il lavoro è disponibile alla seguente repository:						*
*	https://bitbucket.org/bgiacomo/cg-pacman									*
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "stdafx.h"
#include <windows.h>
#include <stdlib.h>
#include <gl/glut.h>
#include <gl/gl.h>
#include <gl/glu.h>
#include <gl/glui.h>
#include <math.h>
#include <stdarg.h>
#include <string>       
#include <iostream>     
#include <sstream>
#include <stdio.h>

using namespace std;

//dimensioni dello schermo su cui sto lavorando
int larghezza;
int altezza;
//variabile booleana che mi permette di capire se il fantasma è arrivato in fondo al percorso per poi farlo ripartire
bool fantasmaArrivato=false;
bool fantasmaArrivatoDue=false;
//variabile booleana necessaria per evitare di ridisegnare pallini già mangiati da pacman nella prima riga
bool primaRigaCompletata=false;
//variabile che memorizza il numero di pallini mangiati da pacman. Utile per il calcolo del punteggio
int numeroPalliniMangiati=0;
//variabile necessaria per fare partire il suono di vittoria solo una volta. Potrebbe essere anche un bool.
int vittoria=0;
//In caso di vittoria il testo sullo schermo sarà lampeggiante. Ciò è possibile grazie a questa variabile: quando vale true la scritta è gialla, viceversa la scritta è nera.
bool testoOnOff=true;

//punti ottenuti per ogni pallino mangiato da pacman
#define INC_PUNTEGGIO 10
//pigreco: indispensabile per disegnare la circonferenza di pacman
#define PI 3.1415926535897932384626433832795
//raggio del cercio
static int radius =100;
//numero di segmenti disegnati per simulare la circonferenza.Maggiore è questo numero e più il poligono sembrerà simile ad un cerchio
static int num_segments = 100;
//il punteggio di pacman
int punteggio=0;
//indica la sensibilità con cui far sparire i pallini dopo che pacman li ha mangiati.
int deltaMangiato=50;

//partenza di Pacman
int posx=150;
int posy=260;

//partenza fantasma
int xGhost=500;
int yGhost=260;

//partenza fantasma rosso
int xGhostRed=2035;
int yGhostRed=1080;
//variabile utile per disegnare i fantasmini
bool primoGiro=false;

//indica la lunghezza del "passo" di pacman
int velocita=20;
//booleano utile a simulare l'animazione della bocca
bool boccaAperta=true;
//utile per avviare il suono di intro solo all'inizio
bool musicaPrimaVolta=true;
int verso=0; // 0=dx, 1=sx, 2=su, 3=giu

//funzione che restituisce false quando pacman incontra un muro del labirino
bool controllaConfini();
//funzione che stampa a video una determinata stringa
void output(int x, int y, int font, char *string);
//funzione che disegna i pallini per pacman
void disegnaPallini2();
//fantasma verde
void fantasmino();
//funzione che fa muovere il fantasma verde con un movimento predefinito
void muoviFantasma();
//funzione che fa muovere il fantasma rosso con un movimento predefinito
void muoviFantasmaRosso();
//funzione chiamata quando pacman incrocia un fantasma. Tutti i parametri vengono ripristinati come all'inizio del gioco
void reset();
//funzione usata per gli effetti sonori
void musica(int suono);

//struct che rappresenta un singolo pallino nello schermo. Il bool disegna indica se il pallino è già stato mangiato o meno
typedef  struct{
	int x;
	int y;
	bool disegna;
	
} Pallino;

//array che memorizza tutti i pallini del gioco
Pallino array[25];

//funzione che stampa a video una determinata stringa
void output(int x, int y, char *string)
{
	glColor3f(255.0,255.0,255.0 );
	glRasterPos2f(x, y);
	int len, i;
	len = (int)strlen(string);
	for (i = 0; i < len; i++) {
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_10, string[i]);
	}
}
//funzione che stampa a video una determinata stringa (sto giro però passo direttamente una stringa e non un array di char)
void output2(int x, int y, string string)
{
	glColor3f(255.0,255.0,255.0 );
	glRasterPos2f(x, y);
	int len, i;
	len = string.length();
	for (i = 0; i < len; i++) {
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_10, string[i]);
	}
}
//funzione che controlla quando pacman incontra il fantasma verde
void gestisciCollisione(){
	
	if((posy==yGhost) && ((posx*2)-50)==xGhost){
		PlaySound(TEXT("morto.wav"), NULL, SND_SYNC);
		reset();
	}
	
	if((posy==380 && yGhost==500)&& ((posx*2)-50)==xGhost){
		PlaySound(TEXT("morto.wav"), NULL, SND_SYNC);
		reset();
	}
	
	if((posy==400 && yGhost==500)&& ((posx*2)-50)==xGhost){
		PlaySound(TEXT("morto.wav"), NULL, SND_SYNC);
		reset();
	}
}

//funzione che controlla quando pacman incontra il fantasma verde
void gestisciCollisioneRosso(){
	
	if((xGhostRed-posx==1265) && (yGhostRed-posy==640)){
		PlaySound(TEXT("morto.wav"), NULL, SND_SYNC);
		reset();
	}
}
//funzione che fa muovere il fantasmino rosso 
void muoviFantasmaRosso(){
	
	
	printf("xRossa: %d, yRossa: %d\n",xGhostRed,yGhostRed);
	if(!fantasmaArrivatoDue){
		if(xGhostRed==2035 && (yGhostRed>=1080 && yGhostRed<1900)){
			yGhostRed+=10;
		}
		if(yGhostRed==1900 && (xGhostRed>2030 && xGhostRed<2310)){
			xGhostRed+=10;
		}
		if(xGhostRed==2315 && (yGhostRed>550 && yGhostRed<1980)){
			yGhostRed-=10;
		}
		if(yGhostRed==550 && (xGhostRed>300 && xGhostRed<=2315)){
			xGhostRed-=10;
		}if(xGhostRed==295 && yGhostRed==550){
			fantasmaArrivatoDue=true;
		}
	}else{
		if(yGhostRed==550 && (xGhostRed>=295 && xGhostRed<2315)){
			xGhostRed+=10;
		}
		if(xGhostRed==2315 && (yGhostRed>=550 && yGhostRed<1980)){
			yGhostRed+=10;
		}
		if(yGhostRed==1980 && (xGhostRed>2035 && xGhostRed<=2315)){
			xGhostRed-=10;
		}
		if(xGhostRed==2035 && (yGhostRed>1080 && yGhostRed<=1980)){
			yGhostRed-=10;
		}
		if(xGhostRed==2035 && yGhostRed==1080){
			fantasmaArrivatoDue=false;
		}
	}
}
//funzione che fa muovere il fantasmino verde
void muoviFantasma(){
	Sleep(10);
	gestisciCollisione();
	gestisciCollisioneRosso();
		printf("xVerde: %d, yVerde: %d\n",xGhost,yGhost);
	if(!fantasmaArrivato){

		if(xGhost<2100 && yGhost==260){
			xGhost+=10;
		}
		else if(xGhost>=2100 && yGhost<500){
			yGhost+=10;
		}else if(yGhost==500 && xGhost>300){
			xGhost-=10;
		}else{
		fantasmaArrivato=true;
		}
	}else{
		if(xGhost<=2100 && yGhost==260){
		xGhost-=10;
			if(xGhost==290 && yGhost==260){
			fantasmaArrivato=false;
			}
		}
		else if(xGhost>=2100 && yGhost<=500){
		yGhost-=10;
		}else if(yGhost==500 && xGhost>=300){
		xGhost+=10;
		}else{

		}
	}
}
//riempio il mio array con tutti i pallini per l'inizio del gioco
void popolaArrayPallini(){
	
	int x1=400;
	int y1=530; //val originale=530
	
	for (int i=0;i<9;i++){
		
		Pallino pal;
		pal.x=x1;
		pal.y=y1;
		pal.disegna=false;
		
		array[i]=pal;
		
		x1+=200;
	}
	
	Pallino pal2;
	pal2.x=1900;
	pal2.y=750;
	pal2.disegna=false;
	
	Pallino pal3;
	pal3.x=2100;
	pal3.y=750;
	pal3.disegna=false;
	
	array[9]=pal2;
	array[10]=pal3;
	
	Pallino pal4;
	pal4.x=1700;
	pal4.y=750;
	pal4.disegna=false;
	
	array[11]=pal4;
	
	Pallino pal5;
	pal5.x=1750;
	pal5.y=950;
	pal5.disegna=false;
	
	array[12]=pal5;
	
	Pallino pal6;
	pal6.x=1700;
	pal6.y=1100;
	pal6.disegna=false;
	
	array[13]=pal6;
	
	
	Pallino pal7;
	pal7.x=1500;
	pal7.y=1100;
	pal7.disegna=false;
	
	array[14]=pal7;
	
	Pallino pal8;
	pal8.x=1350;
	pal8.y=1100;
	pal8.disegna=false;
	
	array[15]=pal8;
	
	Pallino pal9;
	pal9.x=1350;
	pal9.y=1300;
	pal9.disegna=false;

	array[16]=pal9;
	
	Pallino pal10;
	pal10.x=1100;
	pal10.y=1350;
	pal10.disegna=false;
	
	array[17]=pal10;
	
	Pallino pal11;
	pal11.x=1100;
	pal11.y=1500;
	pal11.disegna=false;
	
	array[18]=pal11;
	
	Pallino pal12;
	pal12.x=1100;
	pal12.y=1650;
	pal12.disegna=false;
	
	array[19]=pal12;
	
	int x2=1100;
	int y2=1770;
	
	for(int j=20;j<=24;j++){
		Pallino p;
		p.x=x2;
		p.y=y2;
		p.disegna=false;
		
		x2-=200;
		
		array[j]=p;
	}
	
}

//Ogni pallino contiene una variabile che mi indica se è stato mangiato o meno. Grazie a questo controllo posso calcolare quanti pallini pacman ha mangiato e di conseguenza il punteggio.
int calcolaPalliniMangiati(){

	int j=0;
	for(int i=0;i<24;i++){
		
		if(array[i].disegna){
						
			j++;
		}
	}
	return j;
}
//disegno i pallini dentro il labirinto dopo che pacman ne ha mangiati alcuni (infatti viene fatto il controllo sulla variabile "disegna" del singolo pallino).
void disegnaPallini2(){
	
	glPointSize(12.0);
	glColor3f(0.0, 255.0, 255.0);

	for(int i=0;i<24;i++){
		
		if(!array[i].disegna){
						
			glBegin(GL_POINTS);
			glVertex2i(array[i].x, array[i].y);
			glEnd();
		}
	}
	
	glFlush();
	glMatrixMode(GL_MODELVIEW); //mi permette di entrare nella matrice di modellazione
	glLoadIdentity(); //carico la matrice identità
	
	
	
}
//imposta la visibilità dei pallini a seconda della posizione di pacman
void controllaPosizione(){
	
	glPointSize(12.0);
	glColor3f(0.0, 255.0, 255.0);

	if((posx>210-deltaMangiato && posy>=260) || primaRigaCompletata){
		array[0].disegna=true;
		
		
	}
	
	if((posx>310-deltaMangiato && posy>=260) || primaRigaCompletata){
		array[1].disegna=true;
		
		
	}

	if((posx>430 && posy>=260) || primaRigaCompletata){
		array[2].disegna=true;
		
	}
	
	
	if((posx>490 && posy>=260) || primaRigaCompletata){
		array[3].disegna=true;
		
	}
	
	
	if((posx>590 && posy>=260) || primaRigaCompletata){
		array[4].disegna=true;
		
	}
	
	
	if((posx>690 && posy>=260) || primaRigaCompletata){
		array[5].disegna=true;
		
	}
	
	
	if((posx>790 && posy>=260) || primaRigaCompletata){
		array[6].disegna=true;
		
	}
	
	
	if((posx>890 && posy>=260) || primaRigaCompletata){
		array[7].disegna=true;
		
	}
	
	
	if((posx>950 && posy>=260) || primaRigaCompletata){
		array[8].disegna=true;
		primaRigaCompletata=true;
		
		
	}
	
	
		//SECONDA RIGA
	
	
	if(posy>=360){
		array[9].disegna=true;
	}
	
	
	if(posx<=950 && posy>=360){
		array[10].disegna=true;
		
	}
	
	
	if(posx<=870 && posy>=360){
		array[11].disegna=true;
		
	}
	
		//TERZA RIGA
	
	
	if(posx<=870 && posy>=480){
		array[12].disegna=true;
		
	}
	
	
	if(posx<=870 && posy>=540){
		array[13].disegna=true;
		
	}
	
	
	if(posx<=770 && posy>=540){
		array[14].disegna=true;
		
	}
	
	
	if(posx<=690 && posy>=540){
		array[15].disegna=true;
		
	}
	
		//QUARTA RIGA
	
	
	if(posx<=770 && posy>=640){
		array[16].disegna=true;
	}
	
	
	if(posx<=570 && posy>=660){
		array[17].disegna=true;
		
	}
	
	if(posx<=550 && posy>=740){
		array[18].disegna=true;
		
	}
	
	
	if(posx<=570 && posy>=820){
		array[19].disegna=true;
		
	}
	
		//ULTIMA RIGA
	
	
	if(posx<=570 && posy>=880){
		array[20].disegna=true;
		
	}
	
	
	if(posx<=470 && posy>=880){
		array[21].disegna=true;
		
	}
	
	
	if(posx<=370 && posy>=880){
		array[22].disegna=true;
	}
	
	if(posx<=270 && posy>=660){
		array[23].disegna=true;
		
	}
	
	
	if(posx<=170 && posy>=660){
		array[24].disegna=true;
		
	}
	
}

//funzione che disegna il labirinto
void disegnaLabirinto(){
	
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_LINES);
	
		//disegno righe
	glVertex2i(200, 400);
	glVertex2i(2250, 400);
	
	glVertex2i(200, 650);
	glVertex2i(1850, 650);
	
	
	glVertex2i(200, 900);
	glVertex2i(1550, 900);
	
	glVertex2i(1800, 900);
	glVertex2i(2000, 900);
	
	glVertex2i(200, 1150);
	glVertex2i(1250, 1150);
	
	glVertex2i(400, 1400);
	glVertex2i(950, 1400);
	
	glVertex2i(200, 1650);
	glVertex2i(950, 1650);
	
	glVertex2i(200, 1900);
	glVertex2i(2250, 1900);
	
	glVertex2i(200, 1900);
	glVertex2i(2250, 1900);
	
		//disegno colonne
	
	glVertex2i(200, 650);
	glVertex2i(200, 1650);
	
	glVertex2i(2250, 400);
	glVertex2i(2250, 1900);
	
	glVertex2i(2000, 900);
	glVertex2i(2000, 1600);
	
	glVertex2i(1800, 900);
	glVertex2i(1800, 1900);
	
	glVertex2i(400, 1400);
	glVertex2i(400, 1650);
	
	glVertex2i(1550, 1900);
	glVertex2i(1550, 1250);
	
	glVertex2i(1250, 1900);
	glVertex2i(1250, 1450);
	
	glEnd();
	
	glFlush();
	
}

//funzione che mi serve a fare stare pacman dentro il labirinto: quando questa funzione ritorna false non viene permesso a pacman di muoversi (vedi funzioni muovi dx,sx,su e giu).
bool controllaConfini(){
	
	if(posy<260 || posy>900 || posx>1060  ||posx<150){
		return false;
	}
	if(posx<930 && posx>=150 && posy==300){
		return false;
	}
	if(posx<930 && posx>=150 && posy==360){
		return false;
	}
	
	if(posx<810 && posx>=150 && posy==420){
		return false;
	}
	if(posx<810 && posx>=150 && posy==480){
		return false;
	}
	if(posx<650 && posx>=150 && posy==540){
		return false;
	}
	if(posx<650 && posx>=150 && posy==600){
		return false;
	}
	if(posx<510 && posx>=230 && posy==660){
		return false;
	}
	if(posx<510 && posx>=200 && posy==720){
		return false;
	}
	if(posx<=450 && posx>=150 && posy==780){
		return false;
	}
	if(posy<=760 && posy>=740 && posx==230){
		return false;
	}
	if(posy<=760 && posy>=740 && posx==170){
		return false;
	}
	if(posx<=1010 && posx>=910 && posy==420){
		return false;
	}
	if(posx<=1010 && posx>=910 && posy==480){
		return false;
	}
	if(posy<=900 && posy>=720 && posx==650){
		return false;
	}
	if(posy<=900 && posy>=720 && posx==570){
		return false;
	}
	if(posy<=900 && posy>=640 && posx==730){
		return false;
	}
	if(posy<=900 && posy>=640 && posx==810){
		return false;
	}
	if(posy<=900 && posy>=440 && posx==870){
		return false;
	}
	if(posy<=900 && posy>=440 && posx==930){
		return false;
	}
	if(posy<=840 && posy>=440 && posx==970){
		return false;
	}
	if(posy<=840 && posy>=440 && posx==1030){
		return false;
	}
	
	return true;
}

//funzione per disegnare pacmans
void pacman() {

	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(255.0,255.0,0.0);
	
	glBegin(GL_POLYGON);
	for(double i = 0; i < 2 * PI; i += PI / 13){ //<-- Change this Value
		
		float x=cos(i)*radius;
		float y=sin(i)*radius;
		
		if(x+y==100.000000 && boccaAperta){
			
			x=0.000000;
			y=0.000000;
			
		}
		
		glVertex2i(x+posx, y+posy);
	}
	//piccolo trucco per simulare l'animazione della bocca: quando è false viene disegnata l'intera circonferenza, quando è true viene disegnata la bocca.
	//l'alternarsi di queste due condizioni simula una semplice animazione della bocca
	boccaAperta=!boccaAperta;
	
	printf("posx=%d,posy=%d\n",posx,posy);
	
	glEnd();
	
	glFlush();


	
}

void idle(){
	
	
	glutPostRedisplay();
	
	
	glutSwapBuffers();
	
}

void struttura(){
	
		//ISTRUZIONE IMPORTANTE: tramite questa scalatura posso adattare il programma per tutti gli schermi!
	glScaled(0.3, 0.3, 0.3);
		//Tronco
	glPushMatrix();
	
	glTranslatef(posx, posy, 0.0); //traslo
	
	switch (verso) {
		case 0:
				//destra
			if(controllaConfini()){
				glTranslatef(posx, posy, 0.0);
				glRotatef(0.0, 0.0,1.0, 0.0);
				glTranslatef(-posx, -posy, 0.0);
			}else{
				posx-=velocita;
			}
			break;
		case 1:
				//sinistra
			if(controllaConfini()){
				glTranslatef(posx, posy, 0.0);
				glRotatef(180.0, 0.0,1.0, 0.0);
				glTranslatef(-posx, -posy, 0.0);
			}else{
				posx+=velocita;
			}
			break;
		case 2:
				//su
			if(controllaConfini()){
				glTranslatef(posx, posy, 0.0);
				glRotatef(90.0, 0.0,0.0, 1.0);
				glTranslatef(-posx, -posy, 0.0);
			}else{
				posy-=velocita;
			}
			break;
		case 3:
				//giu
			if(controllaConfini()){
				glTranslatef(posx, posy, 0.0);
				glRotatef(270.0, 0.0,0.0, 1.0);
				glTranslatef(-posx, -posy, 0.0);
			}else{
				posy+=velocita;
			}
			break;
			
	}
	
	pacman();
	if(musicaPrimaVolta){
		musica(1);
		musicaPrimaVolta=false;
	}
	
	glPopMatrix();
	
}
//disegno il fantasma verde
void fantasmino(){
	
	//L'idea di base nel realizzare il fantasmino consiste nel disegnare una semicirconferenza (infatti il ciclo for contiene la variabile PI e non 2*PI come Pacman) ed alla base di questa semicirconferenza agganciare 3 piccoli triangoli

	glPushMatrix();
	glColor3f(0.0,1.0,0.0);
	glScalef(0.3, 0.3, 0.0);
	glTranslatef(0.0, 270.0, 0.0);
	glPushMatrix();
	
	glBegin(GL_POLYGON);
	float x,y;
	float xPrimo,yPrimo;
	bool primoGiro=true;
	for(double i = 0; i <  PI; i += PI / 13){ 
		
		x=cos(i)*radius;
		y=sin(i)*radius;
		glVertex2i(x+xGhost, y+yGhost);
		
		if(primoGiro){
			xPrimo=x+xGhost;
			yPrimo=y+yGhost;
			primoGiro=!primoGiro;
		}
		
	}
	
	glEnd();
	
	glPopMatrix();
	
	glPushMatrix();
	glRecti(x+xGhost, y+yGhost, xPrimo, yPrimo-50);
	glPopMatrix();
	//qui disegno i triangoli da appiccicare alla semicirconferenza precedentemente disegnata
	glPushMatrix();
	glBegin(GL_TRIANGLES);
	glVertex2i(xPrimo, yPrimo);
	glVertex2i(xPrimo-150, yPrimo);
	glVertex2i(xPrimo, yPrimo-100);
	glEnd();
	glBegin(GL_TRIANGLES);
	glVertex2i(xPrimo, yPrimo);
	glVertex2i(xPrimo-200, yPrimo);
	glVertex2i(xPrimo-100, yPrimo-100);
	glEnd();
	glBegin(GL_TRIANGLES);
	glVertex2i(x+xGhost, y+yGhost);
	glVertex2i(x+150+xGhost, y+yGhost);
	glVertex2i(x+xGhost, y-100+yGhost);
	glEnd();
	glFlush();
	
	muoviFantasma();
	glPopMatrix();
	
	glPopMatrix();
	
	
	
}


//disegno il fantasma rosso: il codice è analogo al primo fantasma, cambia solo il colore.
void fantasminoRosso(){
	glPushMatrix();
	glColor3f(1.0,0.0,0.0);
	glScalef(0.28, 0.28, 0.0);
	glPushMatrix();
	
	glBegin(GL_POLYGON);
	float x,y;
	float xPrimo,yPrimo;
	bool primoGiro=true;
	for(double i = 0; i <  PI; i += PI / 13){ 
		
		x=cos(i)*radius;
		y=sin(i)*radius;
		glVertex2i(x+xGhostRed, y+yGhostRed);
		
		if(primoGiro){
			xPrimo=x+xGhostRed;
			yPrimo=y+yGhostRed;
			primoGiro=!primoGiro;
		}

	}
	
	glEnd();
	
	glPopMatrix();
	
	glPushMatrix();
	glRecti(x+xGhostRed, y+yGhostRed, xPrimo, yPrimo-50);
	glPopMatrix();
	
	glPushMatrix();
	glBegin(GL_TRIANGLES);
	   glVertex2i(xPrimo, yPrimo);
	   glVertex2i(xPrimo-150, yPrimo);
	   glVertex2i(xPrimo, yPrimo-100);
	glEnd();
	glBegin(GL_TRIANGLES);
	   glVertex2i(xPrimo, yPrimo);
	   glVertex2i(xPrimo-200, yPrimo);
	   glVertex2i(xPrimo-100, yPrimo-100);
	glEnd();
	glBegin(GL_TRIANGLES);
	   glVertex2i(x+xGhostRed, y+yGhostRed);
	   glVertex2i(x+150+xGhostRed, y+yGhostRed);
	   glVertex2i(x+xGhostRed, y-100+yGhostRed);
	glEnd();
	glFlush();
	
	muoviFantasmaRosso();
	glPopMatrix();
	
	glPopMatrix();
	
	
	
}

//convertire intero a stringa: utile per stampare a video il punteggio
string convertIntero(int valore){

	stringstream ss;
	ss << valore;
	return ss.str();
}

//funzione usata per disegnare la scritta grande pacman
void disegnaTestoStroke(float x, float y, string text){

	glPushMatrix();
	glColor3f(255.0,255.0,0.0);
	glTranslatef(x,y,0.0);
	for (int i = 0;i < text.length();i++){
		glutStrokeCharacter(GLUT_STROKE_ROMAN,text[i]);
	}
	glPopMatrix();
}
//funzione usata per disegnare il sottotitolo e le varie diciture: "You won","You lose" ecc
void disegnaTestoBitmap(float x, float y, string text){

	glPushMatrix();
	glColor3f(0.0,0.0,255.0);
	glRasterPos2f(x,y);
	for (int i = 0;i < text.length();i++){
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, text[i]);
	}
	glPopMatrix();
}
//stessa funzinoe di prima, cambia solo il carattere utilizzato e la grandezza
void disegnaTestoBitmap2(float x, float y, string text){

	glPushMatrix();
	glColor3f(255.0,255.0,0.0);
	glRasterPos2f(x,y);
	for (int i = 0;i < text.length();i++){
		glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, text[i]);
	}
	glPopMatrix();
}
//funzione usata per disegnare la scritta "You lose"
void disegnaTestoLampeggiante(float x, float y, string text){


	glPushMatrix();
	if(testoOnOff){
	glColor3f(255.0,255.0,0.0);
	testoOnOff=!testoOnOff;
	}else{
	glColor3f(0.0,0.0,0.0);
	testoOnOff=!testoOnOff;
	}
	glRasterPos2f(x,y);
	for (int i = 0;i < text.length();i++){
		glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, text[i]);
	}
	Sleep(200);
	glPopMatrix();
}


void display(){

	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(0.13,0.54,0.13);
	glRecti(0.0,0.0,larghezza,400);
	glColor3f(0.59,0.29,0.0);
	
	
	glMatrixMode(GL_MODELVIEW); //mi permette di entrare nella matrice di modellazione
	glLoadIdentity(); //carico la matrice identit‡
	
	struttura();
	disegnaLabirinto();
	
	popolaArrayPallini();
	controllaPosizione();
	disegnaPallini2();
	
	//i fantasmi li faccio apparire quando pacman raggiunge una certa posizione, per garantire un piccolo vantaggio al giocatore
	if(posx>500 || posy>300){
		fantasmino();
		muoviFantasma();
		fantasminoRosso();
		muoviFantasmaRosso();
	}
	//calcolo il punteggio come risultato del numero di pallini mangiati per la define INC_PUNTEGGIO
	punteggio=calcolaPalliniMangiati()*INC_PUNTEGGIO;
	//passaggio necessario per stampare il punteggio a video
	string s =convertIntero(punteggio);
	

	output(100, 100, "r = reset");
	output(100, 80, "Credits: Giacomo Bartoli e Michele Sapignoli");
	
	output(100, 60, "Punteggio: ");
	output2(150, 60,s);
	disegnaTestoStroke(130.0,700.0,"Pacman");
	disegnaTestoBitmap(200.0,650.0,"Collect all points and run away from ghosts");

	//in caso di vittoria il punteggio massimo sarà 240
	if(punteggio==240){
		
		disegnaTestoLampeggiante(250.0,300.0,"You won!\n Press r to restart");
	}
	//arrivato alla fino faccio partire la musichetta di vittoria
	if(posx<250 && (posy==880 || posy==900) && vittoria==0){
		PlaySound(TEXT("vittoria.wav"), NULL, SND_NOWAIT);
		vittoria=1;
	}

	glFlush();
	
	
}

//funzione che sposta pacman a sinistra
void spostaSinistra(){
	verso=1;
	posx-=velocita;
}
//funzione che sposta pacman a destra
void  spostaDestra(){
	verso=0;
	
	if(controllaConfini()){
		posx+=velocita;
	}
}
//funzione che sposta pacman su
void spostaSu(){
	verso=2;
	
	if(controllaConfini()){
		posy+=velocita;
	}
	
}
//funzione che sposta pacman giu
void spostaGiu(){
	verso=3;
	
	if(controllaConfini()){
		posy-=velocita;
	}
	
}
//funzione chiamata per resettare il gioco. Tutte le variabili globali vengono reimpostate.
void reset(){
	
	posx=150;
	posy=260;
	xGhost=500;
	yGhost=260;
	xGhostRed=2035;
	yGhostRed=1080;
	primoGiro=false;
	primaRigaCompletata=false;
	boccaAperta=true;
	musicaPrimaVolta=true;
	punteggio=0;
	vittoria=0;
	glutPostRedisplay();
	glFlush();

}
//catturo l'input da tastiera
void myKeyboard(int key, int x, int y) {
	switch(key){
		case GLUT_KEY_LEFT:
			spostaSinistra();
			break;
			
		case GLUT_KEY_RIGHT:
			spostaDestra();
			break;
			
		case GLUT_KEY_UP:
			spostaSu();
			break;
			
		case GLUT_KEY_DOWN:
			spostaGiu();
			break;
	}
}
//catturo l'input da tastiera dell'utente quando spinge determinate lettere
void keyboard(unsigned char key,int x, int y){

	switch(key){

		case 'r':
			reset();
			break;
			
		case  's':
			Sleep(19000);
			break;
	}


}

void myinit (void)
{
		//per passare dal rgb a glcolor devo dividere ogni colore per 256
	glClearColor (0.0, 0.0, 0.0, 0.0);
	glColor3f(1.0,0.0,0.0);
	glPointSize(4.0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity ( );
	gluOrtho2D(0.0,float(larghezza),0.0,float(altezza));

	

}

void musica(int suono){
	
	if(suono==1){
	PlaySound(TEXT("intro.wav"), NULL, SND_ASYNC);
	}else{
		PlaySound(TEXT("morso.wav"), NULL, SND_ASYNC);

	}
	
}

int main(int argc, char** argv)
{

	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	larghezza = glutGet(GLUT_SCREEN_WIDTH)-870;
	altezza = glutGet(GLUT_SCREEN_HEIGHT)-50;
	glutInitWindowSize(larghezza,altezza);
	glutInitWindowPosition(200,50);
	glutCreateWindow("Pacman simulator");
	
	
	glutDisplayFunc(display);

	glutIdleFunc(idle);
	glutSpecialFunc(myKeyboard);
	glutKeyboardFunc(keyboard);
	myinit();
	
	glutMainLoop();

	return 0;
	
}



