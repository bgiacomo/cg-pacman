# README #

A pacman game simulator. It has been written using openGL libraries.

### Details ###

Computer Science and Engineering, University of Bologna

Computer Graphic course

[Info about the course](http://www.unibo.it/en/teaching/course-unit-catalogue/course-unit?annoAccademico=2014&idComponenteAF=385075)

### Libraries ###

To make sure everything works fine, please run the code on Visual Studio 2010.

### Copyrights ###

Credits: 

* Michele Sapignoli 

* Giacomo Bartoli

Sounds are taken from http://www.classicgaming.cc/classics/pacman/sounds.php

For any question, please contact:

* giacomo.bartoli3@studio.unibo.it

* michele.sapignoli3@studio.unibo.it